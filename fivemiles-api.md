## Summary

We use REST API as much as possible. JSON is the default data format.

## Basic Definition

**name**: username, unique, could be used for login  
**email**: user's email,   
**nick_name**: display name, not unique  
**user_id**: internal uid, integer, unique  
**pass**: user's password


### Quick Reference

### Request Format
**Endpoint Base URL**:  
http://biz.5milesapp.com/api/v1  (BIZ)  
http://item.5milesapp.com/api/v1  (ITEM)  
http://message.5milesapp.com/api/v1	(MESSAGE)  

**Client Request**  
**curl** -X POST|PUT|DELETE|GET -H "X-FIVEMILES-USER-ID:98756" -H "X-FIVEMILES-USER-TOKEN:adfg2344" -d "var=val" endpoint

**I18n Header Field**  
we use standard http header **Accept-Language** to identify the preferred language for the api response, the value is language code, like ***en***, ***zh-cn***, etc. The default value is en-us.

**POST** : create new object  
**DELETE** : delete an object  
**PUT** : modify an object  
**GET** : query object set  

### Response Format

HTTP status code:  

* 200     OK  
    *normal*: json data  
    *error*:  
        {  
            "request":"/path/to/someresource/",  
            "error_code":1202,  
            "error_msg":"Need your follow id"  
        }  
* 201	  Created  
* 400     Bad Request  
* 401     Unauthorized  
* 403     Forbidden  
* 404     Not Found  
* 500     Internal Server Error  
* 501     Not Implemented  
* 503     Service Unavailable

## Auth 

**[Base URL: ITEM]**  

**POST /token/get_passcode/**  
Desc: Register as a new user  
Params:  
* mobile_phone  	string  (eg: 18319876654)   
* mobile_phone_country  string  (eg: 86)  
Response:  
{
	"is_new_user":true
}

**POST /token/verify_passcode/**  
Desc: Verify the passcode received through SMS  
Params:  
* mobile_phone  	string   
* mobile_phone_country  string  
* passcode		string  
Response:
{  
	"token":"12345",  
	"user_id":10,  
	"is_new":true,  
	"expires_at": 1433839105  
}	 

**POST /token/refresh_token/**    
Desc: refresh token before expiration    
Params:  
* mobile_phone  	string   
* mobile_phone_country  string  
Response:
{  
	"token":"23456",  
	"user_id":10,  
	"expires_at": 1433839105  
}

**POST /commodity/user/edit_profile/**   
Desc: Edit self profile, needed for completing registration  
Params:  
* portrait  	image file   
* nick_name		string  
Response: HTTP 200

## Home 
**[Base URL: ITEM]**  

**GET /commodity/home/?lat=5.4&lon=67.3**  
Desc: list all items within 5 miles range  
Response:  
{
    "meta":{
        "limit":20,
        "next":"http://item.5milesapp.com/commodity/home/?lat=5.4&lon=67.3&page=2",
        "offset":0,
        "previous":null,
        "total_count":125
    },
    "objects": [
        {
            "id": 1,
            "owner": {
                "id": 5,
                "username": "bellandtest",
                "first_name": "bellandtest",
                "last_name": "",
                "last_login": "2014-05-27T05:21:12Z",
                "profile": {
                    "id": 5,
                    "nick_name": "bellandtest",
                    "portrait": "http://fivemiles.s3.amazonaws.com/media/profile/green.png",
                    "about": "intro",
                    "language": "en",
                    "country": "us",
                    "seller_positive_rating": 0,
                    "seller_total_rating": 0,
                    "seller_rating_score": 0,
                    "buyer_positive_rating": 0,
                    "buyer_total_rating": 0,
                    "buyer_rating_score": 0
                }
            },
            "title": "Consumer Electronics 2",
            "description": "bellandtest product 1 description",
            "longitude": 180,
            "latitude": 50,
            "update_time": "2014-06-04T07:55:51Z",
            "added_time": "2014-04-17T08:42:16Z",
            "status": 4,
            "price_mode": 1,
            "fixed_price": 1,
            "bidding_start_time": "2014-04-17T08:42:16Z",
            "bidding_end_time": "2014-04-17T08:42:16Z",
            "num_in_stock": 1,
            "image": "http://fivemiles.s3.amazonaws.com/media/commodity_product/2014/06/18/Chrysanthemum.jpg",
            "image_width": 1024,
            "image_height": 768,
            "distance": "0.0 km"
        }
    ]
}


## Search
**[Base URL: ITEM]**  

**GET /commodity/search/?q=desk**  
Desc: search items with keyword  
Response:
    Same as /home/

**GET /commodity/search/?cat=some_cat_slug**  
Desc: get items for this category  
Response: Same as /home/

**GET /commodity/category/**  
Desc: get the categories list  
Response: 
{
    "meta": {
        "next": "http://item.5milesapp.com/commodity/category/?page=2",
        "previous": null,
        "total_count": 17,
        "limit": 12,
        "offset": 0
    },
    "objects": [
        {
            "id": 2,
            "slug": "consumer-electronics",
            "title": "Consumer Electronics",
            "titles": "Consumer Electronics",
            "_order": 0,
            "parent": null
        }，
        {
            "id": 35,
            "slug": "flashlights-lasers",
            "title": "Flashlights & Lasers",
            "titles": "Flashlights & Lasers",
            "_order": 1,
            "parent": null
        }        
    ]
} 


## Product
**[Base URL: ITEM]**  

**GET /commodity/product_detail/1/**  
Desc: get details of an item  
Response:  
{
    "id": 1,
    "owner": 5,
    "title": "Consumer Electronics 2",
    "description": "bellandtest product 1 description",
    "longitude": 180,
    "latitude": 50,
    "update_time": "2014-06-04T07:55:51Z",
    "added_time": "2014-04-17T08:42:16Z",
    "status": 4,
    "price_mode": 1,
    "fixed_price": 1,
    "bidding_start_time": "2014-04-17T08:42:16Z",
    "bidding_end_time": "2014-04-17T08:42:16Z",
    "num_in_stock": 1,
    "images": [
                {
                    "id": 20,
                    "url": "http://fivemiles.s3.amazonaws.com/media/commodity_product/2014/06/18/Desert.jpg",
                    "image_name": "image1",
                    "image_width": 1024,
                    "image_height": 768
                },
                {
                    "id": 21,
                    "url": "http://fivemiles.s3.amazonaws.com/media/commodity_product/2014/06/18/Lighthouse.jpg",
                    "image_name": "image2",
                    "image_width": 1024,
                    "image_height": 768
                }
    ],
    "categories": [
        {
            "id": 2,
            "slug": "consumer-electronics",
            "title": "Consumer Electronics",
            "titles": "Consumer Electronics",
            "_order": 0,
            "parent": null
        }
    ]
}  
** status code **  
0:Invalid  
1:Unpublished  
2:Listing  
3:In transaction  
4:Closed  
5:Delisted  
6:Forbidden  

**[Base URL: BIZ]**  

**POST /watch/**   
Desc: watch an item  
Params:  
* item_id int	
Response: HTTP 201

**POST /unwatch/**    
Desc: unwatch an item  
Params:  
* item_id int	
Response: HTTP 200

**GET /item/1/check_watched/**   
Desc: check if an item has been watched by the request user  
Response: HTTP 200  
{'result':true}

**GET /item/1/questions/**  
Desc: get the question asked about this item  
Response:    
[
    {
        "body": "how r u",
        "timestamp": 1402037575,
        "id": 5,
        "author": {
            "nick_name": "",
            "id": 7,
            "portrait": ""
        }
    }
]

**GET /item/1/offerthreads/**  
Desc: get the offer threads about this item  
Response:
[
    {
        "buyer": {
            "nick_name": "",
            "id": 7,
            "portrait": "/site_media/media/profile/head.png"
        },
        "id": 1,
        "desc": "You sold the item!"
    }
]  

**GET /offerthread/1/**  
Desc: get the offer list for specific offer thread  
Response:
[
    {
        "price": 0,
        "comment": "when to pick up?",
        "from_buyer": true,
        "id": 5,
        "timestamp": 1402472859
    },
    {
        "price": 0,
        "comment": "Offer accepted with price 88.00",
        "from_buyer": true,
        "id": 4,
        "timestamp": 1402468897
    },
    {
        "price": 88,
        "comment": "i'll give you more",
        "from_buyer": false,
        "id": 3,
        "timestamp": 1402467810
    },
    {
        "price": 100,
        "comment": "deserve more!",
        "from_buyer": false,
        "id": 2,
        "timestamp": 1402467579
    },
    {
        "price": 83.5,
        "comment": "looks awesome!",
        "from_buyer": true,
        "id": 1,
        "timestamp": 1402467476
    }
]

**POST /make_offer/**  
Desc: make an offer for the item   
Params:  
* item_id 	int		
* price		float  
* comment 	string		(optional)  
Response:  HTTP 201  
{
	"new_id": 3
}

**POST /counter_offer/**  
Desc: make a counter offer for the item   
Params:  
* ref_offer_id 	int		
* price		float  
* comment 	string		(optional)  
Response:  HTTP 201
{
	"new_id": 5
}

**POST /accept_offer/**  
Desc: accept the offer  
Params:  
* offer_id	int    
Response:  HTTP 200

**POST /send_thread_message/**  
Desc: send message to the other side after closing the deal    
Params:  
* thread_id	int   
* message	string 
Response:  HTTP 201
{
	"new_id": 5
}

**POST /post_question/**  
Params:  
* item_id 	int		
* body	string  
* ref_post_id	int (optional)   
Response:  HTTP 201  
{
	"new_id": 5
}

**POST /post_reply/**  
Params:  
* item_id 	int		
* body	string  
* ref_post_id	int   
Response:  HTTP 201  
{
	"new_id": 5
}

**POST /feedback/**  
Params:  
* item_id  int  
* ratee_id	int		(the target user id)    
* rate_num  int  
* comment	string  
Response:  HTTP 201  

## Account
**[Base URL: ITEM]**  

**POST /heartbeat/**  
Desc: let the server know the user is active  
Response: HTTP 200

**GET /commodity/products_selling/**  
Desc: get the listed items for self  
Response:  
{
    "meta": {
        "next": null,
        "previous": null,
        "total_count": 2,
        "limit": 12,
        "offset": 0
    },
    "objects": [
        {
            "id": 17,
            "owner": 5,
            "title": "3d 2",
            "description": "",
            "longitude": 180,
            "latitude": 50,
            "update_time": "2014-06-18T04:00:37Z",
            "added_time": "2014-06-18T04:00:37Z",
            "status": 2,
            "price_mode": 0,
            "fixed_price": 16,
            "currency_code": "USD",
            "bidding_start_time": "2014-06-18T04:00:37Z",
            "bidding_end_time": "2014-06-18T04:00:37Z",
            "num_in_stock": 1,
            "images": [
                {
                    "id": 20,
                    "url": "http://fivemiles.s3.amazonaws.com/media/commodity_product/2014/06/18/Desert.jpg",
                    "image_name": "image1",
                    "image_width": 1024,
                    "image_height": 768
                },
                {
                    "id": 21,
                    "url": "http://fivemiles.s3.amazonaws.com/media/commodity_product/2014/06/18/Lighthouse.jpg",
                    "image_name": "image2",
                    "image_width": 1024,
                    "image_height": 768
                }
            ],
            "categories": [
                {
                    "id": 3,
                    "slug": "consumer-electronics/3d",
                    "title": "3D",
                    "titles": "Consumer Electronics / 3D",
                    "_order": 0,
                    "parent": 2
                }
            ]
        },
        {
            "id": 16,
            "owner": 5,
            "title": "3d video",
            "description": "",
            "longitude": 180,
            "latitude": 45,
            "update_time": "2014-06-18T03:58:22Z",
            "added_time": "2014-06-18T03:58:22Z",
            "status": 2,
            "price_mode": 0,
            "fixed_price": 15,
            "currency_code": "USD",
            "bidding_start_time": "2014-06-18T03:58:22Z",
            "bidding_end_time": "2014-06-18T03:58:22Z",
            "num_in_stock": 1,
            "images": [
                {
                    "id": 19,
                    "url": "http://fivemiles.s3.amazonaws.com/media/commodity_product/2014/06/18/Chrysanthemum.jpg",
                    "image_name": "image1",
                    "image_width": 1024,
                    "image_height": 768
                }
            ],
            "categories": [
                {
                    "id": 4,
                    "slug": "consumer-electronics/3d/video-glasses",
                    "title": "Video Glasses",
                    "titles": "Consumer Electronics / 3D / Video Glasses",
                    "_order": 0,
                    "parent": 3
                }
            ]
        }
    ]
}

**GET /commodity/user/5/products_selling/**   
Desc: get the listed items for any other user  
Response:
same as above

**GET /commodity/user/5/**   
Desc: get the detailed info of a user  
Response:  
{
	"id":5,
	"nick_name":"John Doe",
	"portrait":"http://app_server/path/to/head.img",
	"about":"I am Iceman!"
}

**[Base URL: BIZ]**  

**GET /user/5/stats/**   
Desc: get the stats info about a user
Response:  
{
	"id":5,
	"count_followers":76,
	"count_followings":24,
	"count_watchings":4,
	"count_sellings":8,
	"count_buyings":2,
	"avg_rate_as_seller:"4.5,
	"avg_rate_as_buyer":3.6
}

**GET /user/5/seller_feedbacks/**  
Desc: get a list of recent feedbacks for an user  
Response:  
[
    {
        "id":101,
        "rate_num":4,
        "comment":"good guy",
        "rater":{"nick_name":"J******e", "portrait":"/static/image/head001.png"}
    },
    {
        "id":102,
        "rate_num":2,
        "comment":"mean guy",
        "rater":{"nick_name":"M******n", "portrait":"/static/image/head002.png"}
    }
]

**GET /watchings/**    
Desc: get a list of watching items for self  
Response: HTTP 200  
[
    {
        "status": 2,
        "update_time": "2014-06-26T05:21:10Z",
        "fixed_price": 25,
        "description": "58863",
        "bidding_start_time": "2014-06-26T05:21:10Z",
        "title": "58886",
        "bidding_end_time": "2014-06-26T05:21:10Z",
        "categories": [],
        "longitude": 150,
        "latitude": 80,
        "images": [
            {
                "url": "http://fivemiles.s3.amazonaws.com/media/commodity_product/2014/06/26/17e5d242-77f2-4c4a-8981-46a1f64591a3.jpg",
                "image_width": 440,
                "image_height": 586,
                "id": 99,
                "image_name": "image1"
            }
        ],
        "price_mode": 0,
        "num_in_stock": null,
        "added_time": "2014-06-26T05:21:10Z",
        "owner": {
            "username": "mb_86_86_10",
            "profile": {
                "about": null,
                "language": null,
                "nick_name": null,
                "country": null,
                "seller_rating_score": 0,
                "buyer_rating_score": 0,
                "seller_total_rating": 0,
                "buyer_total_rating": 0,
                "seller_positive_rating": 0,
                "buyer_positive_rating": 0,
                "portrait": "",
                "id": 20
            },
            "first_name": "",
            "last_name": "",
            "id": 20
        },
        "id": 84,
        "currency_code": "1"
    }
]  

**GET /buyings/**    
Desc: get the buying and bought items for self  
Response:  
same as above

**POST /remark_app/**   
Desc: post user remark info related to this app to the server  
Params:  
* body	string  
* email	 string   
Response: HTTP 200  

## List New Product
**[Base URL: ITEM]**  

**POST /commodity/upload/**  
Desc: List new item on the market  
Params:  
* title		string  
* description		string (optional)  
* fixed_price		float  
* currency_code		string (default='USD')
* latitude		float  
* longitude		float  
* address		string  
* image1		image file  
* price_mode    int     (must be 0 for now)  
* ship_mode		int  
* categories    int  
Response:  HTTP 201    
{
	"new_id": 789,  
	"image_urls":['http://s3.5milesapp.com/image/item1_1.png']
}

**POST /commodity/edit/5/**    
Desc: Edit an item on the market  
Params:  
* title		string  
* description		string  
* fixed_price		float  
* latitude		float  
* longitude		float  
* image1-clear		int  
Response:  HTTP 200  
{
	"id":5
}  

## Followship
**[Base URL: BIZ]**  

**GET /user/5/followers/**  
Desc: Get the followers of a user  
Response:  
{
    "meta":{
        "limit":20,
        "next":"?offset=40&limit=20",
        "offset":20,
        "previous":"?offset=0&limit=20",
        "total_count":63
    },
    "objects": [
        {
            "id":101,
            "nick_name":"Tony",
            "portrait":"/images/avatar/user5.jpg/",
            "followed":true
        }
    ]
}

**GET /user/5/followings/**  
Desc: Get the followers of a user  
Response:  
Same as /followers/

**POST /follow/**  
Desc: follow a guy on the market  
Params:   
* user_id  int		target user uid  
Response: HTTP 201  

**POST /unfollow/**   
Desc: unfollow a guy on the market  
Params:   
* user_id  int		target user uid  
Response: HTTP 200  

**GET /user/5/check_followed/**  
Desc: Check if the request user has followed a user  
Response:  
{
    "result": true
}

##Report  
**[Base URL: BIZ]**  

**GET /report_item_reasons/**  
Desc: get the reason choices for item report  
Response: HTTP 200  
[
    "Miscategorised",
    "Prohibited/Violates terms of use",
    "Spam/Scam/Fraud",
    "Offensive/Inappropriate",
    "Overposting"
]

**GET /report_user_reasons/**  
Desc: get the reason choices for user report  
Response: HTTP 200  
[
    "Abuse/harassment",
    "Violating terms of use",
    "Inappropriate feedback",
    "Spam/Scam/Fraud",
    "Offensive/Inappropriate conduct"
]

**POST /report_item/**  
Desc: report a product as spam  
Params:    
* item_id: int  
* reason:	int  	
Response: HTTP 201  

**POST /report_user/**  
Desc: report a product as spam  
Params:    
* person_uid: int  	
* reason:	int  	
Response: HTTP 201  

## _Message_ (to be finished for the whole section)
**[Base URL: MESSAGE]**  

**GET /messages/**  
Desc: Get the message list for a user  
Response:  
{  
	"meta": {  
	    "limit": 20,   
	    "offset": 20,   
	    "total_count": 3,  
	    "previous": "?limit=20&offset=0",  
	    "next": "?limit=20&offset=40"  
	},   
	"objects": [  
	    {  
	        "message_type": "SYSTEM", # SYSTEM for system message, THREAD for thread message    
	        "count": 4,  
	        "last_message": "5 miles system messages"    
	    },   
	    {
	        "message_type": "THREAD",   
	        "count": 1,   
	        "last_message": "Uh...",  
            "question_id": 523,  
	        "nick": "Henry",   
	        "item_title": "Glasses",   
	        "item_id": 2222,   
            "item_image": "http://item_image_url",  
            "sell_side": 0,
	        "objects": [ ]
	    }, 
	    {  
	        "message_type": "THREAD",   
	        "count": 1,   
	        "last_message": "Yes...",   
            "question_id": 498,  
	        "nick": "Henry",   
	        "item_title": "some toy",   
	        "item_id": 222,  
            "item_image": "http://item_image_url",  
            "sell_side": 0,   
	        "objects": [ ]  
	    },   
	    {  
	        "message_type": "THREAD",   
	        "count": 2,   
	        "item_title": "some toy",   
            "question_id": null,  
	        "item_id": 456,   
            "item_image": "http://item_image_url",  
            "sell_side": 0,  
	        "objects": [  
	            {  
	                "type": "OFFER", # OFFER, CHAT, FEEDBACK  
                    "id": 999,  
	                "sell_side": 0, # 0:the target is seller, 1:the target is buyer  
	                "price": 350,   
	                "comment": null,   
	                "head": "/image/avatar/henry.png",   
	                "thread_id": 444,   
	                "timestamp": 1401972503  
	            },   
	            {  
	                "type": "CHAT",   
                    "id": 999,  
	                "content": "Sorry...",   
	                "head": "/image/avatar/henry.png",   
	                "thread_id": 879,   
	                "timestamp": 1401972503  
	            }  
	        ]  
	    }  
	]  
}  

**GET /messages/unread_num/**  
Desc: Get the unread message number for a user  
Response:  
{
    "unread_num": 4  
}  

**GET /sys\_messages/**  
Desc: Get the system message list for a user  
Response:  
{
	"meta": {  
	    "limit": 20,   
	    "offset": 0,   
	    "total_count": 4  
	},   
	"objects": [  
	    {  
            "id": 962,  
	        "content": "卖家XXX违规，被限制发布商品",   
	        "timestamp": 1401972503  
	    },   
	    {  
            "id": 344,  
	        "content": "商品XXX被移到其他类目YYY",   
	        "timestamp": 1401972503  
	    },   
	    {  
            "id": 231,  
	        "content": "卖家XXX违规，被限制发布商品",   
	        "timestamp": 1401972475  
	    },   
	    {  
            "id": 67,  
	        "content": "商品XXX被移到其他类目YYY",   
	        "timestamp": 1401972475  
	    }  
	]  
}  

**PUT /sys_message/:id/read**  
Desc: Mark a message as read  
Response: 
	HTTP 200

**PUT /questions/:id/read**  
Desc: Mark a question or reply as read  
Response: 
	HTTP 200

**PUT /feedbacks/:id/read**  
Desc: Mark a feedback as read  
Response: 
	HTTP 200

**PUT /threads/:id/read**  
Desc: Mark a thread as read  
Params:  
- sell_side int   
Response: 
    HTTP 200
	
**POST /clients/bind/**  
Desc: bind client ID got from 个推sdk  
Params:  
- client_id		string  
- os			string ('ios'|'android')  
- os_version	string  
- mobile_phone	string  
Response: HTTP 201

**DELETE /clients/unbind/**    
Desc: unbind client ID got from 个推sdk  
Response: HTTP 200  

