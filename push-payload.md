**SYSTEM：点击跳转到系统消息列表**  
	{  
	"type": "S", # system  
	"text": "blah blah blah..."  
	}  
**CHAT：点击跳转到thread页**  
	{  
	"type": "C", # chat   
	"id": 435, # corresponding entry id from auction  
	"thread": 12345, # thread_id  
	"from": "James",  
	"text": "blah blah blah..."  
	}  
**OFFER：点击跳转到thread页**  
	{  
	"type": "O", # offer     
	"id": 435, # corresponding entry id from auction  
	"thread": 12345 # thread_id  
	"from": "James",  
	"price": 59.99,  
	"comment": "blah blah blah..."  
	}  
**FEEDBACK：点击跳转到thread页**  
	{  
	"type": "F", # feedback     
	"id": 435, # corresponding entry id from auction  
	"thread": 12345 # thread_id  
	"from": "James",  
	"rate": 4,  
	"comment": "blah blah blah..."  
	}  
**QUESTION：点击跳转到商品详情页**  
	{  
	"type": "Q", # question     
	"item": 749, # item_id   
	"from": "James",  
	"reply": true, # true for reply, false for question  
	"text": "blah blah blah..."  
	}  